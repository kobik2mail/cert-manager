variable "chart_version" {
  type        = string
  description = "HELM Chart Version for cert-manager"
  default     = "1.15.0"
}
variable "namespace_name" {
  default = "cert-manager"
}

variable "create_namespace" {
  type        = bool
  description = "(Optional) Create namespace?"
  default     = true
}

variable "k8s_host" {
  type = string
  default = ""
}

variable "cluster_ca_certificate" {
  type = string
  default = ""
}

variable "k8s_exec" {
  type = object({
    api_version = string
    command = string
    args = list(string)
  })
}

variable "wait" {
  description = "Will wait until all resources are in a ready state"
  type        = bool
  default     = true
}

variable "timeout" {
  type        = number
  description = "Time in seconds to wait for any individual kubernetes operation"
  default     = 300
}