locals {
  helm_chart      = "cert-manager"
  helm_repository = "https://charts.jetstack.io"
}
