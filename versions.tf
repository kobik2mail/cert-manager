terraform {
  required_version = ">= 1.0.0"

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "> 0.8"
    }
    kubernetes = {
      source  = "kubernetes"
      version = "> 2.25.0"
    }
    helm = {
      source  = "helm"
      version = "> 2.12.0"
    }
    kubectl = {
      source  = "alekc/kubectl"
      version = ">= 2.0.2"
    }
  }
}
