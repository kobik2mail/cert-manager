resource "helm_release" "cert_manager" {
  chart      = local.helm_chart
  repository = local.helm_repository
  name       = "cert-manager"
  namespace  = var.namespace_name
  version    = var.chart_version

  create_namespace = true

  set {
    name  = "installCRDs"
    value = "true"
  }
}