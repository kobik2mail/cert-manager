provider "kubernetes" {
  host                   = var.k8s_host
  cluster_ca_certificate = base64decode(var.cluster_ca_certificate)

  dynamic "exec" {
    for_each = range(var.k8s_exec == null ? 0 : 1)
    content {
      api_version =  var.k8s_exec.api_version
      command     =  var.k8s_exec.command
      args        =  var.k8s_exec.args
    }
  }
}

provider "helm" {
  kubernetes {
    host                   = var.k8s_host
    cluster_ca_certificate = base64decode(var.cluster_ca_certificate)
  
    dynamic "exec" {
      for_each = range(var.k8s_exec == null ? 0 : 1)
      content {
        api_version = var.k8s_exec.api_version
        command     = var.k8s_exec.command
        args        = var.k8s_exec.args
      }
    }
  }
}
